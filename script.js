var inputNome = document.getElementById('nome');
var inputEstilo = document.getElementById('estilo');
var inputURL = document.getElementById('url');

function adicionar(event){
    event.preventDefault();


  if(edit == null){
    var div = document.createElement("div");
    var apagar = document.createElement("button");
    var editar = document.createElement("button");
    
    var nome = document.createElement("p");
    nome.id = "nome"+id;
    
    var estilo = document.createElement("p");
    estilo.id = "estilo"+id;
    
    var img = document.createElement("img");
    img.id = "img"+id;
    
    var br = document.createElement("br");
    var br1 = document.createElement("br");
    var hr = document.createElement("hr");
    
    nome.innerText = "NOME: "+document.getElementById('nome').value;
    estilo.innerText = "ESTILO: "+document.getElementById('estilo').value;
    apagar.innerText = "APAGAR";
    apagar.id = id;
    editar.innerText = "EDITAR";
    editar.id = id;
    
    apagar.onclick = function(){
      document.getElementById(apagar.id).remove();
    };
    
    editar.onclick = function(){
      inputNome.value = document.getElementById("nome"+editar.id).innerText.replace("NOME: ","");
      inputEstilo.value = document.getElementById("estilo"+editar.id).innerText.replace("ESTILO: ","");
      inputURL.value = document.getElementById("img"+editar.id).src;
      edit = editar.id;
    }

    img.setAttribute("src", document.getElementById('url').value);
    img.setAttribute("style", "width:50px; height:50px");
    div.appendChild(hr);
    div.appendChild(nome);
    div.appendChild(estilo);
    div.appendChild(img);
    div.appendChild(br);
    div.appendChild(apagar);
    div.appendChild(br1);
    div.appendChild(editar);
    div.id = id;
    document.body.appendChild(div);
    id++;

    inputNome.value = "";
    inputEstilo.value = "";
    inputURL.value = "";

  }

  else{
    document.getElementById("nome"+edit).innerText = "NOME: "+document.getElementById('nome').value;
    document.getElementById("estilo"+edit).innerText = "ESTILO: "+document.getElementById('estilo').value;;
    document.getElementById("img"+edit).src = document.getElementById('url').value;
    
    inputNome.value = "";
    inputEstilo.value = "";
    inputURL.value = "";
    
    edit = null;
  }

}
